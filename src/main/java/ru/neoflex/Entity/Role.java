package ru.neoflex.Entity;

import org.springframework.security.core.GrantedAuthority;

public enum  Role implements GrantedAuthority {
    USER; //default

    @Override
    public String getAuthority() {
        return name();
    }
}
