package ru.neoflex.Entity;

public class Article {

    private String title;
    private String author;

    public String getTitile() {
        return title;
    }

    public void setTitile(String titile) {
        this.title = titile;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    /*
    <h1>{{title}}</h1>
    <h3>{{publishDate}}</h3>
    <h3>{{author}}</h3>
    <p>{{body}}</p>
    */
}
