package ru.neoflex.Entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import java.util.Collection;
import java.util.List;

@Data //пердоставляет всем полям get set
@Builder //считается, что конструктор по умолчанию ненужен и нужно самим задать
@NoArgsConstructor //пустой
@AllArgsConstructor //со всеми аргументами
public class User implements UserDetails {
    private List<GrantedAuthority> authorities;
    private String username;
    private String password;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
}
