package ru.neoflex.dao;

import org.springframework.stereotype.Repository;
import ru.neoflex.Entity.Article;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleDAO implements IArticleDAO{

    @Override
    public List<Article> getAllArticles() {
        List<Article> list= new ArrayList<Article>();
        for (int i = 0; i<10; i++){
            Article art = new Article();
            art.setTitile("Article Title " + i);
            art.setAuthor("Petrov "+i);
            list.add(art);
        }
        return list;
    }
}
