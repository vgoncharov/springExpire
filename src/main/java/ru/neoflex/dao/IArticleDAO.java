package ru.neoflex.dao;

import ru.neoflex.Entity.Article;

import java.util.List;

public interface IArticleDAO {
    List<Article> getAllArticles();
}
