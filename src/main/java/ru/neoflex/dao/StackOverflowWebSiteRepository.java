package ru.neoflex.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StackOverflowWebSiteRepository extends JpaRepository<StackOverflowWebSite,String> {


}
