package ru.neoflex.dao;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "stackoverflowwebsite")
@Data //пердоставляет всем полям get set
public class StackOverflowWebSite {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private String id;
    @Column(name = "website")
    private String website;
    @Column(name = "icon_image_url")
    private String iconImageUrl;
//    @Setter
    @Column(name = "title")
    private String title;
 //   @Getter
    @Column(name = "description")
    private String description;
}
