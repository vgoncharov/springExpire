package ru.neoflex.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.neoflex.Entity.Article;
import ru.neoflex.dao.ArticleDAO;
import ru.neoflex.service.ArticleService;
import ru.neoflex.service.IArticlesService;

import java.util.List;

@Controller
public class ArticlesController {

    @Autowired
    private ArticleService articlesService;

    @RequestMapping(value = "/article", method = RequestMethod.GET)
    public String displayArticle(Model model){
        List<Article> articles = articlesService.getAllArticles();
        model.addAttribute("articles", articles);
        return "article";
   }
}
