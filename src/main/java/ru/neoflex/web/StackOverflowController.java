package ru.neoflex.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.neoflex.dao.StackOverflowWebSite;
import ru.neoflex.service.StackOverflowService;

import java.util.List;

//@Controller
@RestController
@RequestMapping("/api/stackoverflow")
public class StackOverflowController {

    @Autowired
    private StackOverflowService stackOverflowService;

    @RequestMapping
  //  @ResponseBody /*результат закодировать и отправить, по умолчанию JSON*/
    public List<StackOverflowWebSite> getListOfProviders(){
        return stackOverflowService.findAll();
    }
}
