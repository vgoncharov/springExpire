package ru.neoflex.web;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.neoflex.dao.StackOverflowWebSite;
import ru.neoflex.service.StackOverflowService;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StackOverflowControllerTest {

    //инжектирование зависимости для теста
    @Mock
    private StackOverflowService stackOverflowService;
    @InjectMocks
    StackOverflowController sut;

    /*
    @Before
    public void setUp() throws Exception {
        sut = new StackOverflowController();
    }
*/
    @Test
    public void getListOfProviders() throws Exception{
        //prepare
        when(stackOverflowService.findAll()).thenReturn(ImmutableList.of());
        //testing
        List<StackOverflowWebSite> listOfProviders = sut.getListOfProviders();
        //validate
        verify(stackOverflowService).findAll();

    }
}