package ru.neoflex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import ru.neoflex.dao.StackOverflowWebSite;
import ru.neoflex.dao.StackOverflowWebSiteRepository;

import java.util.List;

@Service
@ComponentScan("ru.neoflex.dao")
public class StackOverflowService {

    @Autowired
   // @Qualifier("stackOverflowWebSiteRepository")
    private StackOverflowWebSiteRepository repository;
/*
    private static List<StackOverflowWebSite> items = new ArrayList<>();
    static {
        items.add(new StackOverflowWebSite(
                "stackoverflow",
                "http://stackoverflow.com",
                "http://cdn.sstatic.net/Sites/stackoverlflow/img/favicon.ico","Stack overflow (StackExchange)",
                "for profession and enthusiast programmers"));
        items.add(new StackOverflowWebSite(
                "stackoverflow222",
                "http://stackoverflow.RU",
                "http://cdn.sstatic.net/Sites/stackoverlflow/img/favicon.ico","Stack overflow (StackExchange)",
                "for easy programmers"));
    }
*/

    public List<StackOverflowWebSite> findAll(){
        return repository.findAll();
    }
}
