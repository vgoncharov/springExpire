package ru.neoflex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.neoflex.Entity.Article;
import ru.neoflex.dao.IArticleDAO;

import java.util.List;

@Service
public class ArticleService implements IArticlesService {

    @Autowired
    private IArticleDAO articleDAO;

    @Override
    public List<Article> getAllArticles() {
        return articleDAO.getAllArticles();
    }
}
