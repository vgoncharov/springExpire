package ru.neoflex.service;

import com.google.common.collect.ImmutableList;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.neoflex.Entity.Role;
import ru.neoflex.Entity.User;
import ru.neoflex.dao.UserDao;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDetails loadUserByUsername(@NonNull  String s) throws UsernameNotFoundException {

       // return userDao.findByUserName(s).orElse(null);
        //Доделать с аунтефикацией по таблице юзеров

        return User.builder()
                .username(s) //логин может быть любым
                .password("pwd") //пароль только pwd
                .authorities(ImmutableList.of(Role.USER))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build(); //создать объект

   }
}
