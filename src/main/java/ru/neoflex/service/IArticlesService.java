package ru.neoflex.service;

import ru.neoflex.Entity.Article;

import java.util.List;

public interface IArticlesService {
    List<Article> getAllArticles();
}
